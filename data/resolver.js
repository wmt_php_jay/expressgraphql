const { User } = require('../models/User');
const { Post } = require('../models/Post');
const { PubSub } = require('graphql-subscriptions');
const pubsub = new PubSub();
const NOTIFICATION_SUBSCRIPTION_TOPIC = 'newNotifications';

const notifications = [];
const resolvers = {
    Query: {
        getUsers: async () => { return await User.find({}).exec() },
        getSingleUser: async (_, args) => { return await User.findById({ '_id': args.id }).exec() },
        getPost: async (_, args) => {
            return await Post.find({}).populate('author').exec()
        },
        getSinglePost: async (_, data) => { return await Post.findById({ '_id': data.id }).populate('author').exec() },
        notifications: () => notifications
    },
    Mutation: {
        addUser: async (_, args) => {
            try {
                const result = await User.create({ userName: args.userName, email: args.email });
                return result;
            } catch (err) {
                return err;
            }
        },
        addPost: async (_, args) => {
            try {

                const result = await Post.create({ title: args.title, author: args.author });
                return result;
            } catch (err) {
                return err;
            }
        },
        updatePost: async (_, args) => {
            try {
                const result = await Post.findByIdAndUpdate({ _id: args.id }, { $set: { title: args.title, author: args.author } }, { new: true });
                return result;
            } catch (err) {
                return err;
            }
        },
        pushNotification: (root, args) => {
           
            const newNotification = { label: args.label };
            notifications.push(newNotification);
    
            return newNotification;
          },

    },
    Subscription: {
        newNotification: {
          subscribe: () => pubsub.asyncIterator(NOTIFICATION_SUBSCRIPTION_TOPIC)
        }
      },
};


module.exports = resolvers