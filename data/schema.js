const { gql } = require('apollo-server-express');



const typeDefs = gql`
    type User {
        id: ID!
        userName: String
        email: String
        password:String
    }
    type Post {
        id: ID!
        title: String
        author:User!
    }
    type Query {
        getUsers: [User]
        getPost:[Post]
        getSingleUser(id:String):User
        getSinglePost(id:String):Post
        notifications: [Notification]
    }
    type Notification { label: String }
    type Mutation {
        addUser(userName: String!, email: String!): User
        addPost(title: String!, author: String!): Post
        updatePost(id:String,title:String!,author:String!):Post
        pushNotification(label: String!): Notification
    }
    type Subscription { newNotification: Notification }
`;

module.exports = typeDefs