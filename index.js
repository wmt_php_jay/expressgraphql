
const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const cors = require('cors')
const { createServer } = require('http');
const http = require('http');
const logger = require('morgan');
const bodyParser = require('body-parser');
const typeDefs = require('./data/schema')
const resolvers = require('./data/resolver')
require('./config');
const server = new ApolloServer({ typeDefs, resolvers });
const app = express();
app.use(cors())
app.use(logger('dev'));
app.use(bodyParser.json());
server.applyMiddleware({ app, path: '/graphql' });
const PORT = 4000;
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);


httpServer.listen(PORT, () => {
  console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`)

})