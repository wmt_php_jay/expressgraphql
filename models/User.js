const mongoose = require('mongoose');
const { Schema } = mongoose;
var uniqueValidator = require('mongoose-unique-validator');
const userSchema = new Schema({
  userName: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },

});



userSchema.plugin(uniqueValidator);
const User = mongoose.model('User', userSchema, 'users');

module.exports = {
  User,
 
};