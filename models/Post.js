const mongoose = require('mongoose');
const { Schema } = mongoose;
var uniqueValidator = require('mongoose-unique-validator');

const PostSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    title: { type: String, required: true },

});

PostSchema.plugin(uniqueValidator);
const Post = mongoose.model('Post', PostSchema, 'posts');

module.exports = {
    Post
};

